import {
  StyleSheet, Text, View, Dimensions, FlatList, Alert,
} from 'react-native';
import React, { useState, useEffect, useCallback } from 'react';
import axios from 'axios';
import moment from 'moment';
import PropTypes from 'prop-types';
import { primaryColor } from '../../utils/colors';
import {
  Poster, Backdrop, Tag, Title, CastItem,
} from '../../components';
import {
  h3, h4, h5, h6,
} from '../../utils/fontSize';

const window = Dimensions.get('screen');

const styles = StyleSheet.create({
  lineContainer: {
    height: 30,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(255,255,255,.9)',
    borderTopLeftRadius: 50,
    borderTopRightRadius: 50,
    overflow: 'hidden',
    marginTop: -30,
  },
  line: {
    width: 50,
    height: 3,
    borderRadius: 2,
    backgroundColor: 'black',
  },
  contentContainer: {
    marginHorizontal: 24,
    marginTop: 24,
    marginBottom: 12,
    flexDirection: 'row',
  },
  contentTextContainer: {
    flex: 1.08,
    marginHorizontal: 24,
    marginVertical: 12,
    justifyContent: 'center',
  },
  textTitle: {
    color: 'black',
    fontFamily: 'Poppins-Bold',
    fontSize: h3,
  },
  textInfo: {
    color: 'black',
    fontFamily: 'Poppins-Light',
    fontSize: h5,
    marginTop: -6,
  },
  textTagline: {
    color: 'black',
    fontFamily: 'Poppins-Medium',
    fontSize: h4,
    textAlign: 'center',
    marginTop: 8,
  },
  releaseContainer: {
    backgroundColor: primaryColor,
    marginVertical: 12,
    paddingHorizontal: 18,
    flexDirection: 'row',
    alignSelf: 'center',
  },
  textDate: {
    fontFamily: 'Poppins-Light',
    fontSize: h6,
    color: 'black',
  },
  genresContainer: {
    marginHorizontal: 24,
    flexWrap: 'wrap',
    flexDirection: 'row',
  },
  textGenre: {
    fontFamily: 'Poppins-Regular',
    fontSize: h4,
    color: 'black',
  },
  textSynopsis: {
    fontFamily: 'Poppins-Medium',
    color: 'black',
    fontSize: 18,
    marginHorizontal: 36,
  },
  castContainer: {
    paddingHorizontal: 36,
    alignItems: 'center',
    alignContent: 'center',
    paddingBottom: 24,
  },
});

function Detail({ route, navigation }) {
  const { id } = route.params;

  const [isLoading, setLoading] = useState(true);
  const [movie, setMovie] = useState();

  const getMovieData = useCallback(() => {
    setLoading(true);
    axios
      .get(`http://code.aldipee.com/api/v1/movies/${id}`)
      .then(({ data }) => {
        setMovie(data);
      })
      .finally(() => {
        setLoading(false);
      })
      .catch((err) => {
        Alert(err);
      });
  });

  useEffect(() => {
    getMovieData();
  }, []);

  if (!movie) return null;

  const header = (
    <View style={{ marginHorizontal: -36 }}>
      <Backdrop
        imgUrl={movie.backdrop_path}
        navigation={navigation}
        title={movie.title}
      />
      <View style={styles.lineContainer}>
        <View style={styles.line} />
      </View>
      <View>
        <View style={styles.contentContainer}>
          <Poster
            index={movie.id}
            imgUrl={movie.poster_path}
            imgSize={{
              width: window.width * 0.36,
              height: window.width * 0.55,
            }}
          />
          <View style={styles.contentTextContainer}>
            <Text style={styles.textTitle}>{movie.original_title}</Text>
            <Text style={styles.textInfo}>
              {movie.runtime}
              {' '}
              mins |
              {movie.vote_average}
            </Text>
            <Text style={styles.textTagline}>
              {movie.tagline}
            </Text>
            <Tag
              index={movie.id}
              title={`${movie.status}  `}
              title2={(
                <Text style={styles.textDate}>
                  {moment(movie.release_date).format('D MMM YYYY')}
                </Text>
              )}
              style={styles.releaseContainer}
            />
          </View>
        </View>
        <Title title="Genres" style={{ marginBottom: -8 }} />
        <View style={styles.genresContainer}>
          {movie.genres.map((item) => (
            <Tag
              index={item.id}
              title={item.name}
              key={item.id}
              style={{
                paddingHorizontal: 18,
                marginBottom: 12,
              }}
              fontStyle={styles.textGenre}
            />
          ))}
        </View>
        <Title title="Synopsis" style={{ marginBottom: -8 }} />
        <View>
          <Text style={styles.textSynopsis}>{movie.overview}</Text>
        </View>
        <Title title="Cast" style={{ marginBottom: -8 }} />
      </View>
    </View>
  );
  return (
    <FlatList
      onRefresh={getMovieData}
      refreshing={isLoading}
      contentContainerStyle={styles.castContainer}
      numColumns={3}
      data={movie.credits.cast}
      keyExtractor={(item) => item.id}
      showsHorizontalScrollIndicator={false}
      renderItem={({ item }) => (
        <CastItem imgUrl={item.profile_path} index={item.id} title={item.name} key={item.id} />
      )}
      ListHeaderComponent={header}
    />
  );
}

export default Detail;

Detail.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
  }).isRequired,
  route: PropTypes.shape({
    params: PropTypes.shape({ root: PropTypes.string, id: PropTypes.number }),
  }).isRequired,
};
