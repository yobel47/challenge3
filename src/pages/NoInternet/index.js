import {
  View, Dimensions, StatusBar,
} from 'react-native';
import React from 'react';
import LottieView from 'lottie-react-native';
import { Cow } from '../../assets';
import { Header, Title, Button } from '../../components';
import { primaryColor } from '../../utils/colors';

const window = Dimensions.get('screen');

function NoInternet() {
  return (
    <View
      style={{
        justifyContent: 'space-between',
        width: window.width,
        height: window.height,
        paddingBottom: 36,
      }}
    >
      <StatusBar backgroundColor={primaryColor} barStyle="dark-content" />
      <Header />
      <LottieView
        source={Cow}
        style={{
          width: window.height,
          height: window.width,
          marginTop: 10,
        }}
        autoPlay
        loop
      />
      <Title
        title="No Internet Connection"
        style={{
          marginTop: 12,
          alignItems: 'center',
          marginBottom: 12,
        }}
        textStyle={{ fontSize: 32 }}
      />
      <Button text="Try Again" />
    </View>
  );
}

export default NoInternet;
