import Splash from './Splash';
import Home from './Home';
import Detail from './Detail';
import NoInternet from './NoInternet';

export {
  Splash, Home, Detail, NoInternet,
};
