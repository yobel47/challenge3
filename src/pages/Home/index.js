import React, { useEffect, useState, useCallback } from 'react';
import {
  View, StyleSheet, FlatList, Dimensions, StatusBar, Alert,
} from 'react-native';
import axios from 'axios';
import PropTypes from 'prop-types';
import { primaryColor } from '../../utils/colors';
import {
  Header, Title, MovieCarousel, Loading, Card,
} from '../../components';

const window = Dimensions.get('screen');

const styles = StyleSheet.create({
  container: {
    width: window.width,
    height: window.height,
    backgroundColor: 'white',
  },
});

function Home({ navigation }) {
  const [isLoading, setLoading] = useState(true);
  const [latestData, setLatestData] = useState([]);
  const [recommendData, setRecommendData] = useState([]);

  const getMovieData = useCallback(() => {
    setLoading(true);
    axios
      .get('http://code.aldipee.com/api/v1/movies/')
      .then(({ data }) => {
        const { results } = data;
        const latest = results.sort((a, b) => new Date(b.release_date) - new Date(a.release_date));
        setLatestData(latest);
        const recommend = results
          .filter((item) => item.vote_average > 7)
          .sort((a, b) => a.vote_average - b.vote_average)
          .reverse()
          .slice(0, 5);
        setRecommendData(recommend);
      })
      .finally(() => {
        setLoading(false);
      })
      .catch((err) => {
        Alert(err);
      });
  });

  useEffect(() => {
    getMovieData();
  }, []);

  const header = (
    <>
      <Header />
      <Title title="Recommended" />
      <MovieCarousel navigation={navigation} data={recommendData} />
      <Title title="Latest Movies" style={{ marginTop: -12 }} />
    </>
  );

  return (
    <View style={styles.container}>
      <StatusBar backgroundColor={primaryColor} barStyle="dark-content" />
      {isLoading ? (
        <Loading />
      ) : (
        <FlatList
          onRefresh={getMovieData}
          refreshing={isLoading}
          contentContainerStyle={{
            alignContent: 'center',
            paddingBottom: 36,
          }}
          data={latestData}
          keyExtractor={(item) => item.id}
          showsHorizontalScrollIndicator={false}
          renderItem={({ item }) => <Card navigation={navigation} data={item} />}
          ListHeaderComponent={header}
        />
      )}
    </View>
  );
}

export default Home;

Home.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
    goBack: PropTypes.func.isRequired,
  }).isRequired,
};
