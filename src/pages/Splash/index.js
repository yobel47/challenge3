import {
  StyleSheet, Text, View, StatusBar, Dimensions,
} from 'react-native';
import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import LottieView from 'lottie-react-native';
import { primaryColor } from '../../utils/colors';
import { Camera } from '../../assets';

const window = Dimensions.get('screen');

const styles = StyleSheet.create({
  container: {
    width: window.width,
    height: window.height,
    backgroundColor: 'white',
    alignItems: 'center',
  },
  logoContainer: {
    width: window.width,
    alignItems: 'center',
    justifyContent: 'center',
    flex: 9,
  },
  logoBackground: {
    backgroundColor: primaryColor,
    borderRadius: window.width * 0.6,
    padding: window.width * 0.1,
  },
  logoText: {
    fontSize: 48,
    marginTop: 20,
    color: 'black',
    fontFamily: 'Poppins-ExtraBold',
  },
  ccContainer: {
    width: window.width,
    alignItems: 'center',
    flex: 1,
  },
  ccBackground: {
    backgroundColor: primaryColor,
    width: window.width * 0.5,
    height: window.width * 0.5,
    borderRadius: 100,
    alignItems: 'center',
  },
  ccText: {
    color: 'black',
    fontSize: 24,
    marginTop: 2,
    letterSpacing: 4,
    fontFamily: 'Poppins-ExtraBold',
  },
});

function Splash({ navigation }) {
  useEffect(() => {
    setTimeout(() => {
      navigation.replace('MainApp');
    }, 3000);
  });

  return (
    <View style={styles.container}>
      <StatusBar backgroundColor="white" barStyle="dark-content" />
      <View style={styles.logoContainer}>
        <View style={styles.logoBackground}>
          <LottieView
            source={Camera}
            style={{ width: window.width * 0.5, height: window.width * 0.5 }}
            autoPlay
            loop
          />
        </View>
        <Text style={styles.logoText}>Mooveez</Text>
      </View>
      <View style={styles.ccContainer}>
        <View style={styles.ccBackground}>
          <Text style={styles.ccText}>Bell</Text>
        </View>
      </View>
    </View>
  );
}

export default Splash;

Splash.propTypes = {
  navigation: PropTypes.shape({
    replace: PropTypes.func.isRequired,
  }).isRequired,
};
