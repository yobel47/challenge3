import { StyleSheet, Text, View } from 'react-native';
import React from 'react';
import PropTypes from 'prop-types';
import { h6 } from '../../utils/fontSize';
import { secondaryColor } from '../../utils/colors';

const styles = StyleSheet.create({
  container: {
    alignSelf: 'flex-start',
    backgroundColor: secondaryColor,
    paddingHorizontal: 8,
    borderRadius: 20,
    marginBottom: 8,
    marginRight: 8,
  },
  text: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: h6,
    color: 'black',
  },
  textDate: {
    fontFamily: 'Poppins-Light',
    fontSize: h6,
    color: 'black',
  },
});

function Tag({
  title, title2, style, fontStyle, index,
}) {
  return (
    <View style={[styles.container, { ...style }]} key={index}>
      <Text style={[styles.text, { ...fontStyle }]}>
        {title}
      </Text>
      {title2 && !null ? title2 : null}

    </View>
  );
}

export default Tag;

Tag.propTypes = {
  title: PropTypes.string.isRequired,
  title2: PropTypes.shape({ root: PropTypes.string }),
  index: PropTypes.number,
  style: PropTypes.shape({ root: PropTypes.string }),
  fontStyle: PropTypes.shape({ root: PropTypes.string }),
};

Tag.defaultProps = {
  title2: null,
  style: null,
  fontStyle: null,
  index: 0,
};
