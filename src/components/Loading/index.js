import { StyleSheet, View, Dimensions } from 'react-native';
import React from 'react';
import LottieView from 'lottie-react-native';
import { LoadingIcon } from '../../assets';

const window = Dimensions.get('screen');

const styles = StyleSheet.create({
  container: {
    width: window.width,
    height: window.height,
    justifyContent: 'center',
    alignItems: 'center',
  },
  loading: {
    width: window.width,
    height: window.width,
  },
});

function Loading() {
  return (
    <View style={styles.container}>
      <LottieView source={LoadingIcon} style={styles.loading} autoPlay />
    </View>
  );
}

export default Loading;
