import { StyleSheet, View, Dimensions } from 'react-native';
import React from 'react';
import Icon from 'react-native-vector-icons/Ionicons';
import PropTypes from 'prop-types';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { primaryColor } from '../../utils/colors';

const window = Dimensions.get('screen');

const styles = StyleSheet.create({
  container: {
    backgroundColor: primaryColor,
    width: window.width * 0.1,
    height: window.width * 0.1,
    marginHorizontal: 6,
    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

function CircleButton({ name, onPress }) {
  return (
    <TouchableOpacity onPress={onPress}>
      <View style={styles.container}>
        <Icon name={name} size={window.width * 0.05} color="black" />
      </View>
    </TouchableOpacity>
  );
}

export default CircleButton;

CircleButton.propTypes = {
  name: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired,
};
