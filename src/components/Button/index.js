import {
  StyleSheet, Text, TouchableOpacity,
} from 'react-native';
import React from 'react';
import PropTypes from 'prop-types';

const styles = StyleSheet.create({
  button: {
    backgroundColor: 'black',
    paddingHorizontal: 16,
    paddingVertical: 12,
    width: '100%',
    alignItems: 'center',
  },
  buttonText: {
    color: '#fff',
    fontFamily: 'Poppins-Bold',
    fontSize: 24,
  },
});

function Button({ text }) {
  return (
    <TouchableOpacity style={styles.button}>
      <Text style={styles.buttonText}>
        {text}
      </Text>
    </TouchableOpacity>
  );
}

export default Button;

Button.propTypes = {
  text: PropTypes.string.isRequired,
};
