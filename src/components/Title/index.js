import {
  StyleSheet, Text, View, Dimensions,
} from 'react-native';
import React from 'react';
import PropTypes from 'prop-types';
import { h2 } from '../../utils/fontSize';

const window = Dimensions.get('screen');

const styles = StyleSheet.create({
  container: { width: window.width, marginVertical: 12 },
  fontStyle: {
    fontFamily: 'Poppins-Bold',
    color: 'black',
    fontSize: h2,
    marginVertical: 12,
    marginHorizontal: 36,
  },
});

function Title({ title, style, textStyle }) {
  return (
    <View style={[styles.container, { ...style }]}>
      <Text style={[styles.fontStyle, { ...textStyle }]}>{title}</Text>
    </View>
  );
}

export default Title;

Title.propTypes = {
  title: PropTypes.string.isRequired,
  style: PropTypes.shape({ root: PropTypes.string }),
  textStyle: PropTypes.shape({ root: PropTypes.string }),
};

Title.defaultProps = {
  style: null,
  textStyle: null,
};
