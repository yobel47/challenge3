import {
  StyleSheet,
  View,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import React, { useState } from 'react';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import PropTypes from 'prop-types';
import Poster from '../Poster';

const window = Dimensions.get('screen');

const styles = StyleSheet.create({
  dotStyle: {
    width: 10,
    height: 10,
    borderRadius: 5,
    marginHorizontal: -10,
    backgroundColor: 'rgba(0, 0, 0, 0.92)',
  },
});

function MovieCarousel({ navigation, data }) {
  const [activeIndex, setActiveIndex] = useState(0);
  const isCarousel = React.useRef(null);

  return (
    <View>
      <Carousel
        data={data}
        ref={isCarousel}
        renderItem={({ item }) => (
          <TouchableOpacity
            style={{ backgroundColor: 'white', alignItems: 'center' }}
            onPress={() => navigation.navigate('Detail', { id: item.id })}
          >
            <Poster
              imgUrl={item.poster_path}
              index={item.id}
              imgSize={{
                width: window.width * 0.4 - 20,
                height: window.width * 0.6,
              }}
            />
          </TouchableOpacity>
        )}
        sliderWidth={window.width}
        itemWidth={190}
        itemHeight={350}
        onSnapToItem={(index) => setActiveIndex(index)}
        useScrollView
        autoplay
        loop
      />
      <Pagination
        dotsLength={data.length}
        activeDotIndex={activeIndex}
        carouselRef={isCarousel}
        dotStyle={styles.dotStyle}
        inactiveDotOpacity={0.7}
        inactiveDotScale={0.6}
        tappableDots
      />
    </View>
  );
}

export default MovieCarousel;

MovieCarousel.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
  }).isRequired,
  data: PropTypes.arrayOf(PropTypes.shape({
    length: PropTypes.number,
  })).isRequired,
};
