import { StyleSheet, Text, View } from 'react-native';
import React from 'react';
import { h1 } from '../../utils/fontSize';
import { primaryColor } from '../../utils/colors';

const styles = StyleSheet.create({
  containerHeader: {
    overflow: 'hidden',
    width: window.width,
    borderBottomLeftRadius: 500,
    borderBottomRightRadius: 500,
  },
  titleContainer: {
    backgroundColor: primaryColor,
    alignItems: 'center',
    justifyContent: 'center',
  },
  fontStyle: {
    fontFamily: 'Poppins-ExtraBold',
    color: 'black',
    fontSize: h1,
  },
});

function Header() {
  return (
    <View style={styles.containerHeader}>
      <View style={styles.titleContainer}>
        <Text style={styles.fontStyle}>Mooveez</Text>
      </View>
    </View>
  );
}

export default Header;
