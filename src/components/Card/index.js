import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  Pressable,
  Dimensions,
  Alert,
} from 'react-native';
import React, { useState, useEffect } from 'react';
import Icon from 'react-native-vector-icons/Ionicons';
import axios from 'axios';
import moment from 'moment';
import PropTypes from 'prop-types';
import { primaryColor } from '../../utils/colors';
import Poster from '../Poster';
import { h5, h6 } from '../../utils/fontSize';
import Tag from '../Tag';

const window = Dimensions.get('screen');

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 36,
    marginBottom: 24,
    borderRadius: 35,
    overflow: 'hidden',
  },
  textCard: {
    flex: 1.5,
    borderRadius: 30,
    overflow: 'hidden',
    marginVertical: 24,
    marginHorizontal: 24,
    backgroundColor: 'rgba(0,0,0,.8)',
    justifyContent: 'space-between',
  },
  textContainer: {
    flex: 1,
    justifyContent: 'space-between',
    marginHorizontal: 14,
    marginVertical: 18,
  },
  title: {
    fontFamily: 'Poppins-ExtraBold',
    fontSize: h5,
    color: 'white',
  },
  year: { fontFamily: 'Poppins-Light', color: 'white', fontSize: h6 },
  genreContainer: {
    flexWrap: 'wrap',
    flexDirection: 'row',
    marginTop: 12,
  },
  ratingContainer: {
    alignSelf: 'flex-start',
    justifyContent: 'center',
    backgroundColor: 'white',
    width: 35,
    height: 35,
    padding: 4,
    borderRadius: 50,
    marginTop: 4,
  },
  rating: {
    fontFamily: 'Poppins-ExtraBold',
    fontSize: h5,
    color: 'black',
    textAlign: 'center',
    textAlignVertical: 'center',
    paddingTop: 3,
  },
  sendContainer: {
    backgroundColor: primaryColor,
    alignSelf: 'flex-end',
    marginTop: -45,
    marginLeft: -36,
    borderTopLeftRadius: 30,
    overflow: 'hidden',
  },
});

function Card({ navigation, data }) {
  const [genre, setGenre] = useState();

  const getGenreData = async () => {
    try {
      const response = await axios.get(
        `http://code.aldipee.com/api/v1/movies/${data.id}`,
      );
      setGenre(response.data.genres);
    } catch (error) {
      Alert(error);
    }
  };

  useEffect(() => {
    getGenreData();
  }, []);

  if (!genre) return null;

  return (
    <View>
      <Pressable
        onPress={() => navigation.navigate('Detail', { id: data.id })}
        style={({ pressed }) => [
          {
            backgroundColor: pressed ? 'black' : 'white',
          },
          styles.container,
        ]}
      >
        <ImageBackground
          source={{ uri: data.backdrop_path }}
          resizeMode="cover"
          opacity={0.6}
          style={{ flexDirection: 'row' }}
        >
          <Poster
            index={data.id}
            imgUrl={data.poster_path}
            imgSize={{ width: window.width * 0.26, height: window.width * 0.4 }}
            style={{
              flex: 1,
              marginVertical: 24,
              marginLeft: 24,
            }}
          />
          <View style={styles.textCard}>
            <View style={[styles.textContainer, {}]}>
              <Text style={styles.title}>
                {data.original_title}
                {' '}
                <Text style={styles.year}>
                  {' '}
                  {moment(data.release_date).format('YYYY')}
                </Text>
              </Text>
              <View style={styles.genreContainer}>
                {genre.map((item) => <Tag title={item.name} key={item.id} />)}
              </View>
              <View style={styles.ratingContainer}>
                <Text style={styles.rating}>{data.vote_average}</Text>
              </View>
            </View>
            <View style={styles.sendContainer}>
              <Icon
                name="paper-plane"
                size={window.width * 0.08}
                color="black"
                style={{ margin: 4 }}
              />
            </View>
          </View>
        </ImageBackground>
      </Pressable>
    </View>
  );
}

export default Card;

Card.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
  }).isRequired,
  data: PropTypes.shape({
    adult: PropTypes.bool,
    id: PropTypes.number,
    backdrop_path: PropTypes.string,
    poster_path: PropTypes.string,
    original_title: PropTypes.string,
    release_date: PropTypes.string,
    vote_average: PropTypes.number,
  }).isRequired,
};
