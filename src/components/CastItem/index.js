import {
  StyleSheet, Text, View, Dimensions,
} from 'react-native';
import React from 'react';
import PropTypes from 'prop-types';
import { h5 } from '../../utils/fontSize';
import Poster from '../Poster';

const window = Dimensions.get('screen');

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    alignContent: 'center',
    marginVertical: 10,
    width: window.width * 0.25,
    marginHorizontal: 8,
  },
  textStyle: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: h5,
    color: 'black',
    marginTop: 8,
    textAlign: 'center',
  },
});

function CastItem({ imgUrl, title, index }) {
  return (
    <View style={styles.container}>
      <Poster
        index={index}
        imgUrl={imgUrl}
        imgSize={{
          width: window.width * 0.25,
          height: window.width * 0.4,
        }}
      />
      <Text style={styles.textStyle}>{title}</Text>
    </View>
  );
}

export default CastItem;

CastItem.propTypes = {
  imgUrl: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  index: PropTypes.number.isRequired,
};
