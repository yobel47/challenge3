import {
  StyleSheet,
  View,
  ImageBackground,
  Dimensions,
  Share,
  Alert,
} from 'react-native';
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import CircleButton from '../CircleButton';

const window = Dimensions.get('screen');

const styles = StyleSheet.create({
  buttonContainer: {
    height: window.height * 0.05,
    margin: 12,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});

function Backdrop({ imgUrl, navigation, title }) {
  const [nameIcon, setNameIcon] = useState('heart-outline');
  const changeIcon = (icon) => setNameIcon(icon);
  const onShare = async () => {
    try {
      await Share.share({
        message: title,
      });
    } catch (error) {
      Alert(error);
    }
  };

  return (
    <View height={window.height * 0.3}>
      <ImageBackground
        source={{
          uri: imgUrl,
        }}
        style={{
          flex: 1,
        }}
      >
        <View style={styles.buttonContainer}>
          <CircleButton
            name="chevron-back"
            onPress={() => navigation.goBack('Home')}
          />
          <View
            style={{
              flexDirection: 'row',
            }}
          >
            <CircleButton
              name={nameIcon}
              onPress={() => {
                if (nameIcon === 'heart') {
                  changeIcon('heart-outline');
                } else {
                  changeIcon('heart');
                }
              }}
            />
            <CircleButton name="share-social-outline" onPress={onShare} />
          </View>
        </View>
      </ImageBackground>
    </View>
  );
}

export default Backdrop;

Backdrop.propTypes = {
  imgUrl: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
    goBack: PropTypes.func.isRequired,
  }).isRequired,
};
