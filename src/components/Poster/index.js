import {
  StyleSheet, View, Image, ActivityIndicator,
} from 'react-native';
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { NotFound } from '../../assets';

const styles = StyleSheet.create({
  containerItem: {
    borderRadius: 30,
    overflow: 'hidden',
    justifyContent: 'center',
  },
  image: {
    resizeMode: 'stretch',
  },
});

function Poster({
  imgUrl, index, imgSize, style,
}) {
  const [loading, setLoading] = useState(false);
  const [image, setImage] = useState({ uri: imgUrl });
  const [styleImg, setStyleImg] = useState([styles.image, { ...imgSize }]);

  const onError = () => {
    setImage(NotFound);
    setStyleImg({
      width: 80,
      height: 80,
      alignSelf: 'center',
    });
  };

  function onLoading(value) {
    setLoading(value);
  }

  return (
    <View style={[styles.containerItem, { ...imgSize }, { ...style }]} key={index}>
      {loading && (
        <View
          style={{
            justifyContent: 'center',
            alignSelf: 'center',
            alignContent: 'center',
            zIndex: 0,
            width: '100%',
            position: 'absolute',
            height: 350,
          }}
        >
          <ActivityIndicator color="grey" size="large" />
        </View>
      )}
      <Image
        source={image}
        style={styleImg}
        onError={onError}
        onLoadStart={() => onLoading(true)}
        onLoadEnd={() => onLoading(false)}
      />
    </View>
  );
}

export default Poster;

Poster.propTypes = {
  imgUrl: PropTypes.string.isRequired,
  index: PropTypes.number.isRequired,
  imgSize: PropTypes.shape({
    width: PropTypes.number.isRequired,
    height: PropTypes.number.isRequired,
  }).isRequired,
  style: PropTypes.shape({ root: PropTypes.string }),
};

Poster.defaultProps = {
  style: null,
};
