import NotFound from './404.png';
import Cow from './cow.json';
import Camera from './camera.json';
import LoadingIcon from './loading.json';

export {
  NotFound, Cow, Camera, LoadingIcon,
};
